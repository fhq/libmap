# libmap

自动扫描scene/文件夹

遮罩图已按图块区域算好（320*240）

# libgg

基于galaxy2d封装的 标签 主循环 节点 精灵

# libgge

galaxy2d游戏引擎(https://www.cnblogs.com/jianguhan/)

# libjpeg

jpeg解码库(by云风 Cloud Wu)

# libpng

png图像库(from cocos2dx)

# 示意图

![demo](https://gitee.com/ulxy/libmap/raw/master/diagram/demo.png)

![preview](https://gitee.com/ulxy/libmap/raw/master/diagram/all.png)

![block](https://gitee.com/ulxy/libmap/raw/master/diagram/block.png)

![mask](https://gitee.com/ulxy/libmap/raw/master/diagram/mask.png)


# 开发环境

Visual Studio Community 2017

![mask](https://gitee.com/ulxy/libmap/raw/master/diagram/installer.png)